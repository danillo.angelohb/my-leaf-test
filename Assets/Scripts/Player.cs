﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player : SingletonMonoBehaviour<Player>
{  
    private Rigidbody rbody;
    public float walkSpeed;
    public float runSpeed;
    public float acceleration;
    private Vector2 currentSpeed;
    float smoothVelocity;
    public float jumpHeight;
    private Vector3 jump;    
    public float drag = 5f;
    public Vector3 moveRot;
    Vector3 input;
    public bool isJumping;
    //public bool isRunning;
    public float smoothTime = 0.01f;
    public Animator anim;
    public Camera cam;
    public float gravityScale;
    public bool grounded;
    public bool crouch;
    public bool allowInput = true;       

    void Start()
    {
        rbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }
   
    void Update()
    {
        if (allowInput)
        {
            if (transform.position.y < -2.6f) Die();
            input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;            
            anim.SetBool("Walk", input.magnitude > 0 && !crouch);
            anim.SetBool("CrouchWalk", input.magnitude > 0 && crouch);
            // anim.SetBool("Run", isRunning);
            anim.speed = Input.GetKey(KeyCode.LeftShift) ? 2 : 1;
            if (Input.GetButtonDown("Jump") && grounded) isJumping = true;
            if (Input.GetKeyUp(KeyCode.C))
            {
                if (!crouch)
                {
                    crouch = true;
                    anim.SetBool("Crouch", true);
                    anim.SetBool("Walk", false);
                }
                else
                {
                    crouch = false;
                    anim.SetBool("Crouch", false);
                }
            }
        }
        else input = Vector3.zero;
    }

    void Die()
    {
        allowInput = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void FixedUpdate()
    {
        float target;
        float turnSmooth;
        moveRot = new Vector3(0, 0, 0);
        if (input.magnitude > 0)
        {
            target = Mathf.Atan2(input.x, input.z) * Mathf.Rad2Deg + cam.transform.eulerAngles.y;
            turnSmooth = Mathf.SmoothDampAngle(transform.eulerAngles.y, target, ref smoothVelocity, smoothTime);
            transform.rotation = Quaternion.Euler(0, turnSmooth, 0);
            moveRot = Quaternion.Euler(0, target, 0) * Vector3.forward * walkSpeed;
        }
        if (isJumping && grounded)
        {
            currentSpeed.y = jumpHeight;
            isJumping = false;
        }
        if (input.magnitude > 0)
        {
            currentSpeed.x += acceleration * Time.fixedDeltaTime;
            float targetSpeed = Input.GetKey(KeyCode.LeftShift)? runSpeed : walkSpeed;
            if (currentSpeed.x > targetSpeed) currentSpeed.x = targetSpeed;
        }
        else currentSpeed.x = Mathf.Max(0, currentSpeed.x - drag * Time.fixedDeltaTime);
        if (!grounded)
        {
            currentSpeed.y += Physics.gravity.y * gravityScale * Time.fixedDeltaTime;
        }
        rbody.velocity = (transform.forward * currentSpeed.x + Vector3.up * currentSpeed.y);
    }

    public void SetGrounded(bool state)
    {
        grounded = state;
    }    
}