﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public Rigidbody rb;
    public bool isGrab;
    public bool canGrab = true;
    public Transform[] groundChecks;
    public LayerMask mask;
    public bool isGrounded = true;
    public GrabController grabC;
    public string boxName;

    private void Start()
    {
        if (GetStoredPosition(out Vector3 position))
        {
            transform.position = position;
        }
    }
    void Update()
    {
        if (isGrab && canGrab)
        {
            bool foundOne = false;
            foreach (Transform gc in groundChecks)
            {
                Debug.DrawRay(gc.position, 0.3f * Vector3.down, Color.red);
                if (Physics.Raycast(gc.position, Vector3.down, 0.3f, mask))
                {
                    foundOne = true;
                    break;
                }
            }
            isGrounded = foundOne;
            if (!foundOne)
            {
                canGrab = false;
                rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                gameObject.layer = LayerMask.NameToLayer("Ignore Ground");
                isGrab = false;
                grabC.StopGrab();
            }
        }
    }

    public void StartGrab(GrabController grabController)
    {
        //Debug.Log("Start");
        isGrab = true;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        grabC = grabController;
    }

    public void StopGrab()
    {
        rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        isGrab = false;
    }

    public bool GetStoredPosition(out Vector3 position)
    {
        position = PlayerPrefsX.GetVector3(boxName, Vector3.zero);
        return position != Vector3.zero;
    }

    public void StorePosition()
    {
        PlayerPrefsX.SetVector3(boxName, transform.position);
    }
}