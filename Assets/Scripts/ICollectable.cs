﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Collectable : MonoBehaviour
{
    public int value;

    public virtual void Collect()
    {
        int currentCoins = PlayerPrefs.GetInt("Coin", 0);
        PlayerPrefs.SetInt("Coin", currentCoins + value);
        UiController.instance.UpdateCoins();
    }

    private void OnTriggerEnter(Collider other)
    {
        Collect();
        Destroy(gameObject);
    }

}
