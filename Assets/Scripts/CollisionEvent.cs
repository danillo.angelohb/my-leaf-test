﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionEvent : MonoBehaviour
{
    public UnityEvent onTriggerEnter;
    public UnityEvent onTriggerExit;
    public UnityEvent onCollisionEnter;
    public UnityEvent onCollisionExit;   
    public List<Collider> colliders = new List<Collider>();          

    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnter?.Invoke();
        colliders.Add(other);
        for (int i = 0; i < colliders.Count; i++)
        {
            if (colliders[i] == null)
            {
                colliders.RemoveAt(i);
                i--;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        colliders.Remove(other);
        if (colliders.Count == 0) onTriggerExit?.Invoke();
        for (int i = 0; i < colliders.Count; i++)
        {
            if (colliders[i] == null)
            {
                colliders.RemoveAt(i);
                i--;
            }
        }
    }    
}
