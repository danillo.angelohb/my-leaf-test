﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabController : MonoBehaviour
{
    public Vector3 grabDirection;
    public Box box;
    public bool canGrab;
    public bool isLocked;
    public Transform player;
    private Rigidbody rbPlayer;
    public float rayCastDistance;
    public LayerMask mask;
    public Animator anim;
    public float speed;   

    void Start()
    {
        player = Player.instance.transform;
        rbPlayer = Player.instance.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        RaycastHit info;
        Debug.DrawRay(transform.position, player.forward * rayCastDistance, Color.red);
        if (Physics.Raycast(transform.position, player.forward, out info, rayCastDistance, mask))
        {
            canGrab = true;
            box = info.collider.transform.GetComponent<Box>();
            grabDirection = -info.normal;
        }
        else canGrab = false;
        if (canGrab && Input.GetMouseButtonDown(0))
        {
            if (box.canGrab) StartGrab();
        }
        if (isLocked && Input.GetMouseButtonUp(0))
        {
            StopGrab();
        }
    }

    private void FixedUpdate()
    {
        if (isLocked)
        {
            float movement = Input.GetAxisRaw("Vertical");
            if (Mathf.Abs(movement) > 0.1f)
            {
                if (movement > 0) anim.SetFloat("GrabBlend", 1);
                else anim.SetFloat("GrabBlend", 0);
                rbPlayer.velocity = movement * grabDirection * speed;
                box.rb.velocity = movement * grabDirection * speed;
            }
            else
            {
                anim.SetFloat("GrabBlend", 0.5f);
                rbPlayer.velocity = Vector3.zero;
                box.rb.velocity = Vector3.zero;
            }
        }
    }

    void StartGrab()
    {
        if (!box.canGrab) return;
        box.StartGrab(this);
        Player.instance.enabled = false;
        Player.instance.anim.SetBool("Grab", true);
        isLocked = true;
        player.LookAt(player.position + grabDirection);
    }

    public void StopGrab()
    {
        box.StopGrab();
        Player.instance.enabled = true;
        Player.instance.anim.SetBool("Grab", false);
        isLocked = false;
        if (!canGrab)
        {
            box = null;
            grabDirection = Vector3.zero;
        }
    }
}