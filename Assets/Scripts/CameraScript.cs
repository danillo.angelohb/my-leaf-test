﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    Vector3 dist;

    void Start()
    {
        dist = transform.position - Player.instance.transform.position;
    }
   
    void Update()
    {
        transform.position = Player.instance.transform.position + dist;
    }

    public void ChangeCamera()
    {
        transform.position = new Vector3(-23f, 13.57f, -31);
        transform.rotation = Quaternion.Euler(33f, -181f, 0);
        dist = transform.position - Player.instance.transform.position;
    }
}
