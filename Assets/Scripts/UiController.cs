﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiController : SingletonMonoBehaviour<UiController>
{ 
    public Text coin;
    public GrabController grab;
    public GameObject grabButton;
    public GameObject pauseScreen;
    public GameObject defeat;
           
    void Start()
    {
        UpdateCoins();
    }

    // Update is called once per frame
    void Update()
    {
        grabButton.SetActive(grab.canGrab);
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!pauseScreen.activeSelf) Pause();
            else Resume();
        }
    }

    public void UpdateCoins()
    {
        coin.text = PlayerPrefs.GetInt("Coin", 0).ToString();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        pauseScreen.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        pauseScreen.SetActive(false);
    }

    public void Restart()
    {
        Time.timeScale = 1;
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Defeat()
    {
        defeat.SetActive(true);
    }

}
