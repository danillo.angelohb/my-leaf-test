﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    public Transform target;
    private Rigidbody rbody;
    Animator anim;
    public Transform rayOrigin;
    public float walkSpeed;
    public List<Transform> tList;
    public float waitTime;
    bool foundPlayer;
    public float radius;
    public float angle;
    public LayerMask layerMask;
   
    void Start()
    {
        rbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        StartCoroutine(WalkRoutine());
    }     
    
    private void FixedUpdate()
    {
        Vector3 direction = (Player.instance.transform.position - transform.position);
        direction.y = 0;
        direction.Normalize();
        Vector3 look = transform.forward;
        look.y = 0;
        look.Normalize();
        Vector3 yOffset = Vector3.up;
        if (Vector3.Dot(look, direction) > 0.4f || ((Player.instance.transform.position - transform.position).magnitude < 6f && !Player.instance.crouch))
        {
            Debug.DrawRay(rayOrigin.position, direction * radius, Color.red);
            if (Physics.Raycast(rayOrigin.position, direction, out RaycastHit info, radius, layerMask))
            {
                if (info.collider.gameObject.CompareTag("Player") && !foundPlayer)
                {
                    foundPlayer = true;
                    FindPlayer();
                }
            }
        }
    }

    IEnumerator WalkRoutine()
    {
        while (true)
        {
            foreach (Transform target in tList)
            {
                anim.SetBool("Walk", true);
                while (Vector3.Distance(rbody.position, target.position) > 0.1f)
                {
                    Vector3 direction = (target.position - rbody.position).normalized;
                    rbody.velocity = direction * walkSpeed;
                    Vector3 targetRot = new Vector3(target.position.x, transform.position.y, target.position.z);
                    transform.LookAt(targetRot);
                    yield return new WaitForFixedUpdate();
                }
                rbody.velocity = Vector3.zero;
                rbody.angularVelocity = Vector3.zero;
                anim.SetBool("Walk", false);
                yield return new WaitForSeconds(waitTime);
            }
        }
    }

    async void FindPlayer()
    {
        StopAllCoroutines();
        rbody.velocity = Vector3.zero;
        anim.SetBool("Fight", true);
        Player.instance.anim.SetTrigger("Pray");
        Player.instance.allowInput = false;
        Vector3 targetRot = new Vector3(Player.instance.transform.position.x, transform.position.y, Player.instance.transform.position.z);
        transform.LookAt(targetRot);
        UiController.instance.Defeat();
        await Task.Delay(2000);
        SceneManager.LoadScene(0);
    }
}